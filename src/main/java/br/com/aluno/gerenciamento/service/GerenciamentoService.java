package br.com.aluno.gerenciamento.service;

import br.com.aluno.gerenciamento.entities.Aluno;
import br.com.aluno.gerenciamento.exception.AlunoNaoEncontradoException;
import br.com.aluno.gerenciamento.repository.AlunoRepository;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import org.apache.tomcat.jni.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
@RequestMapping("/alunogerenciamento")
public class GerenciamentoService extends Throwable {

    private final AlunoRepository alunoRepository;

    private List<Aluno> alunos;

    public GerenciamentoService(AlunoRepository alunoRepository) {
        this.alunoRepository = alunoRepository;
        this.alunos = new ArrayList<>();
    }

    @GetMapping
    public List<Aluno> findAll(@RequestParam(required = false) String nome, Integer idade) throws URISyntaxException {
        try {
            if (nome != null) {
                return alunos.stream()
                        .filter(aln -> aln.getNome().contains(nome))
                        .collect(Collectors.toList());
            } else {
                if (idade != null) {
                    return alunos.stream().filter(aln -> aln.getIdade().equals(idade)).collect(Collectors.toList());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return alunos;
    }

    public List<Aluno> findAluno(String nome) throws URISyntaxException {
        try {
            List<String> alunos = null;
            alunos.stream()
                    .filter(aluno -> aluno.equalsIgnoreCase(nome))
                    .findFirst()
                    .orElseThrow(() -> new AlunoNaoEncontradoException(nome));
        } catch (AlunoNaoEncontradoException e) {
            e.getMessage();
        }
        return alunos;
    }

    @GetMapping("/{id}")
    public Aluno findById(@PathVariable("id") Integer id){
        return this.alunos.stream().filter(aln -> aln.getId().equals(id))
                .findFirst().orElse(null);
    }

    @PostMapping
    public ResponseEntity<Integer> add(@RequestBody Aluno aluno){
        if(aluno.getId() == null){
            aluno.setId(alunos.size() + 1);
        }
        alunos.add(aluno);
        return new ResponseEntity<>(aluno.getId(), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity update(@RequestBody Aluno aluno){
        alunos.stream().filter(aln -> aln.getId().equals(aluno.getId())).forEach(aln -> aln.setNome(aluno.getNome()));
        alunos.stream().filter(aln -> aln.getId().equals(aluno.getId())).forEach(aln -> aln.setIdade(aluno.getIdade()));
        return new ResponseEntity(HttpStatus.NO_CONTENT);

    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") Integer id){
        alunos.removeIf(aln -> aln.getId().equals(id));
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    public CompletableFuture<Optional<User>>findById(Long id){
        System.out.println("Service Thread: " + Thread.currentThread().getName());
        return alunoRepository.findOneById(id);
    }

    public CompletableFuture<List<User>> findAll(){
        System.out.println("Service Thread: " + Thread.currentThread().getName());
        return this.alunoRepository.findAllBy();
    }

    public CompletableFuture<User> save(final User user){
        System.out.println("Service Thread: " + Thread.currentThread().getName());
        return CompletableFuture.completedFuture(alunoRepository.save(user));
    }

}
