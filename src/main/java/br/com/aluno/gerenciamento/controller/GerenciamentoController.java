package br.com.aluno.gerenciamento.controller;

import br.com.aluno.gerenciamento.exception.AlunoNaoEncontradoException;
import br.com.aluno.gerenciamento.exception.UserNotFound;
import br.com.aluno.gerenciamento.service.GerenciamentoService;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.apache.tomcat.jni.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/aluno")
@RestController
@Async
public class GerenciamentoController {

    private final GerenciamentoService gerenciamentoService;

    Logger LOGGER = (Logger) LoggerFactory.getLogger(GerenciamentoService.class);

    public GerenciamentoController(GerenciamentoService gerenciamentoService) {
        this.gerenciamentoService = gerenciamentoService;
    }

    @ExceptionHandler({AlunoNaoEncontradoException.class})
    public ResponseEntity<String> handle(final AlunoNaoEncontradoException e){
        final String alunoInexistente = "Aluno não encontrado";
        LOGGER.error(alunoInexistente);
        return new ResponseEntity<>(alunoInexistente, HttpStatus.NOT_FOUND);
    }

    @GetMapping
    public ResponseEntity<String>get() throws GerenciamentoService {
        throw new AlunoNaoEncontradoException();
    }

    @GetMapping
    public CompletableFuture<List<User>> findAll(){
        System.out.println("Service Thread: " + Thread.currentThread().getName());
        return this.gerenciamentoService.findAll();
    }

    @GetMapping("/{id}")
    public CompletableFuture<User> findById(@PathVariable("id")final Long id){
        System.out.println("Controller Thread: " + Thread.currentThread().getName());
        return this.gerenciamentoService.findById(id)
                .thenApply(x -> x.orElseThrow(UserNotFound::new));
    }

    @PutMapping
    public ResponseEntity<String>put() throws GerenciamentoService {
        throw new AlunoNaoEncontradoException();
    }

    @PostMapping
    public CompletableFuture<User> save(@RequestBody final User user){
        System.out.println("Controller Thread: " + Thread.currentThread().getName());
        return this.gerenciamentoService.save(user);
    }
}