package br.com.aluno.gerenciamento.repository;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import org.apache.tomcat.jni.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Async;

@Async("customThreadPool")
public interface AlunoRepository extends JpaRepository<User, Long> {

    CompletableFuture<Optional<User>> findOneById(final Long id);

    CompletableFuture<List<User>> findAllBy();

}
