package br.com.aluno.gerenciamento.exception;

public class AlunoNaoEncontradoException extends RuntimeException{

    public AlunoNaoEncontradoException(String nome) {
        super("Nome " + nome + " não encontado.");
    }

    public AlunoNaoEncontradoException() {

    }
}
